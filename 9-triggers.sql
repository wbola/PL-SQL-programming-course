SET SERVEROUTPUT ON;

/*Rozwiń program MODUŁ 5 ZADANIE 2 o autonomiczną procedurę do obsługi błędów. W tabeli logów powinna się znaleźć informacja o dacie wystąpienia błędu, użytkowniku wykonującym operację, kodzie i komunikacie błędu.*/

CREATE TABLE error_log
(
    error_date DATE,
    exec_user VARCHAR2(50),
    error_code VARCHAR2(50),
    error_command VARCHAR2(500)
);

CREATE OR REPLACE PROCEDURE error_log (in_date DATE,
                                       in_user VARCHAR2,
                                       in_code VARCHAR2,
                                       in_command VARCHAR2)
IS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    INSERT INTO error_log (error_date, error_user, error_code, error_command)
    VALUES (in_date, in_user, in_code, in_command);
    
    COMMIT;
END;
/

DECLARE
	ex_constraint EXCEPTION;
	PRAGMA EXCEPTION_INIT(ex_constraint, -00001);
BEGIN
	INSERT INTO COUNTRIES(country_id, country_name) VALUES ('ML', 'Malta');
EXCEPTION
	WHEN ex_constraint THEN
    	error_log(sysdate, user, sqlcode, sqlerrm);
END;
/


/*Zmodyfikuj wyzwalacz złożony(przykład 3 z pliku z kodem z lekcji) w taki sposób by walidował pensję pracownika (80%-120% średniej departamentu) tylko, gdy w tym departamencie danego pracownika pracuje więcej niż 5 osób. W przeciwnym razie zakres 80%-120% licz na podstawie zarobków całej firmy.
Uwaga. Gdy w firmie nie jest zatrudniony żaden pracownik wyzwalacz nie powinien dokonywać żadnej walidacji.
*/

--DROP TRIGGER zlozony_wyzwalacz;
CREATE OR REPLACE TRIGGER zlozony_wyzwalacz
FOR INSERT OR UPDATE OF SALARY ON employees
COMPOUND TRIGGER
    TYPE rec_emp IS RECORD (department_id departments.department_id%TYPE
                          , department_name departments.department_name%TYPE
                          , avg_salary employees.salary%TYPE
                          , count_employee NUMBER);
    TYPE at_emp IS TABLE OF rec_emp INDEX BY PLS_INTEGER;
    v_emp at_emp;
    v_avg_salary_company NUMBER;
    BEFORE STATEMENT
    IS
    BEGIN
        SELECT d.department_id, d.department_name, round(avg(e.salary),2) avg_salary, count(e.employee_id) count_employee
          BULK COLLECT INTO v_emp
          FROM employees e
    INNER JOIN departments d
            ON d.department_id = e.department_id
         GROUP BY d.department_id, d.department_name;
        
    SELECT round(avg(salary),2) 
    INTO v_avg_salary_company
    FROM employees;
    
    END BEFORE STATEMENT;
    
    BEFORE EACH ROW
    IS
    BEGIN
        FOR i IN v_emp.FIRST..v_emp.LAST
        LOOP
            IF 
                v_emp(i).count_employee > 5 AND
                :NEW.department_ID = v_emp(i).department_id AND :NEW.salary NOT BETWEEN v_emp(i).avg_salary * 0.8 AND v_emp(i).avg_salary * 1.2
                THEN
                    RAISE_APPLICATION_ERROR(-20001, ' Średnia pensja w departamencie '||v_emp(i).department_name||' wynosi '||v_emp(i).avg_salary
                    ||'. Nowa pensja pracownika '||:NEW.employee_ID||'('||:NEW.salary||') przekracza dopuszczalne 20% odchylenie od średniej.');
            ELSIF 
                v_emp(i).count_employee <= 5 AND
                :NEW.department_ID = v_emp(i).department_id  AND :NEW.salary NOT BETWEEN v_avg_salary_company * 0.8 AND v_avg_salary_company * 1.2
                THEN
                    RAISE_APPLICATION_ERROR(-20001, ' Średnia pensja w całej firmie wynosi '||v_avg_salary_company
                    ||'. Nowa pensja pracownika '||:NEW.employee_ID||'('||:NEW.salary||') przekracza dopuszczalne 20% odchylenie od średniej.');
            END IF;
        END LOOP;
    END BEFORE EACH ROW;
END zlozony_wyzwalacz;
/

SELECT round(avg(salary),2) avg_salary
     , round(avg(salary),2) * 0.8 min_salary
	 , round(avg(salary),2) * 1.2 max_salary
  FROM employees
 WHERE department_id = 50
 ;

INSERT INTO employees (employee_id, last_name, email, hire_date, job_id, department_id, salary) 
     VALUES (285, 'kursant', 'kontakt@nieinformatyk.pl', sysdate, 'IT_PROG', 50, 9000);

INSERT INTO employees (employee_id, last_name, email, hire_date, job_id, department_id, salary) 
     VALUES (285, 'kursant', 'kontakt@nieinformatyk.pl', sysdate, 'IT_PROG', 10, 9000);   
     
/*Napisz wyzwalacz, który uniemożliwi logowanie się do bazy danych weekendami(sobota i niedziela).
** Dla chętnych. Napisz wyzwalacz, który dodatkowo zabroni logować się do bazy danych poza godzinami 7.00-18.00 w dni robocze(od poniedziałku do piątku). 
*/

CREATE OR REPLACE TRIGGER a_logon_trg
AFTER LOGON ON DATABASE
DECLARE 
v_day_of_week VARCHAR2(20);
v_hour NUMBER;
BEGIN
    SELECT to_char(sysdate, 'Day') 
    INTO v_day_of_week
    FROM dual;
    
    SELECT to_char(sysdate, 'HH24') 
    INTO v_hour
    FROM dual;

    IF v_day_of_week IN ('Sobota', 'Niedziela') THEN
        RAISE_APPLICATION_ERROR(-20001, 'Nie możesz się zalogować do bazy w weekend');
    ELSIF v_hour NOT BETWEEN 7 AND 18 THEN
        RAISE_APPLICATION_ERROR(-20001, 'Nie możesz się zalogować do bazy poza godzinami 7.00-18.00 w dni robocze(od poniedziałku do piątku)');
    END IF;
END a_logon_trg;
/


--robocze
SELECT sysdate FROM dual;

select to_char(sysdate, 'Day') day_of_week
from   dual;

select to_char(sysdate, 'HH24') hour
from   dual;