/*Stwórz tabelę lista_obecnych składającą się z 2 kolumn o nazwie: data oraz liczba_pracowników. Napisz job, który będzie uruchamiał się od poniedziałku do piątku o godzinie 14.30. Celem joba jest zalogowanie do tabeli lista_obecnych informacji o liczbie pracowników będących danego dnia w pracy(liczba rekordów z tabeli employees). 
Uwaga. Joba powinien korzystać ze zdefiniowanego harmonogramu oraz programu(procedury).
*/

CREATE TABLE lista_obecnych
(
    data  DATE,
    liczba_pracowników NUMBER
);

CREATE OR REPLACE PROCEDURE lista_obecności
IS
    v_cnt NUMBER;
BEGIN
    SELECT COUNT(*)
    INTO v_cnt
    FROM employees;

    INSERT INTO lista_obecnych (data, liczba_pracowników) VALUES (sysdate, v_cnt);
END;
/

BEGIN
  DBMS_SCHEDULER.create_program (
    program_name   => 'program_lista_obecności',
    program_type   => 'PLSQL_BLOCK',
    program_action => 'lista_obecności;',
    enabled        => TRUE,
    comments       => 'Program, który dodaje informacje o liczbie pracowników będących danego dnia w pracy do tabeli lista_obecnych.');
END;
/

BEGIN
  DBMS_SCHEDULER.create_schedule (
    schedule_name   => 'harmonogram_od_pon_do_pt_14_30',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI; BYHOUR=14; BYMINUTE=30;',
    end_date        => NULL,
    comments        => 'Powtarzany od poniedziałku do piątku o godzinie 14:30 terminarz.');
END;
/

BEGIN
  DBMS_SCHEDULER.create_job (
    job_name        => 'job_lista_obecności',
    program_name    => 'program_lista_obecności',
    schedule_name   => 'harmonogram_od_pon_do_pt_14_30',
    enabled         => TRUE,
    comments        => 'Logowanie informacji o liczbie pracowników będących danego dnia w pracy do tabeli lista_obecnych.'
    );
END;
/

BEGIN
    DBMS_SCHEDULER.run_job (job_name => 'job_lista_obecności');
END;
/

BEGIN
    DBMS_SCHEDULER.drop_job (job_name => 'job_lista_obecności');
END;
/

BEGIN
    DBMS_SCHEDULER.drop_schedule (schedule_name => 'harmonogram_od_pon_do_pt_14_30');
END;
/

BEGIN
    DBMS_SCHEDULER.drop_program (program_name => 'program_lista_obecności');
END;
/

SELECT *
  FROM all_scheduler_job_run_details
 WHERE job_name = 'JOB_LISTA_OBECNOŚCI'
;

SELECT *
  FROM lista_obecnych
;


/*Dodaj do programu z punktu 1 funkcjonalność uniemożliwiającą zapętlenie się joba, tzn. sprawdź czy job jest aktualnie wykonywany. Jeśli tak, to nie uruchamiaj tego dnia programu.*/

SELECT job_name, status
  FROM all_scheduler_job_run_details;

SELECT *
  FROM dictionary
 WHERE table_name LIKE 'ALL_SCHEDULER%';

SELECT *
  FROM all_scheduler_running_jobs;

CREATE OR REPLACE PROCEDURE lista_obecności
IS
    v_cnt NUMBER;
    v_check_run_job NUMBER;
BEGIN
    SELECT COUNT(*)
    INTO v_check_run_job
    FROM all_scheduler_running_jobs
    WHERE job_name = UPPER('job_lista_obecności');
    
    IF v_check_run_job = 0 
        THEN
            SELECT COUNT(*)
            INTO v_cnt
            FROM employees;

            INSERT INTO lista_obecnych (data, liczba_pracowników) VALUES (sysdate, v_cnt);
    END IF;
END;
/

TRUNCATE TABLE lista_obecnych;