/*Rozwiń program z MODUŁ 8 ZADANIE 5 o próbę kompilacji nieskompilowanych obiektów. Jeśli nie uda Ci się skompilować danego obiektu, tzn. dalej będzie zwracał komunikat błędu to zaloguj informację o błędzie, nazwie i typie obiektu do dedykowanej tabeli logów.
UWAGA. Do kompilacji należy użyć polecenia ALTER object_type object_name COMPILE lub dedykowanego pakietu. https://oracle-base.com/articles/misc/recompiling-invalid-schema-objects*/

DROP TABLE error_log;
CREATE TABLE error_log
(
    error_message VARCHAR2(100),
    object_name   VARCHAR2(100),
    object_type   VARCHAR2(100)
);

CREATE OR REPLACE PROCEDURE skompiluj_nieskompilowane_obiekty
IS
	CURSOR c_obj IS
                    SELECT 
                    object_name, 
                    object_type, 
                    status 
                    FROM user_objects
                    WHERE STATUS = 'INVALID';
	v_c NUMBER := 0;
    v_sqlerrm VARCHAR2(512);
    
BEGIN           	 
	FOR i in c_obj
	LOOP           
		DBMS_OUTPUT.put_line(i.object_name);
		v_c := v_c + 1;
        
        BEGIN
            EXECUTE IMMEDIATE 'ALTER ' ||i.object_type|| ' ' ||i.object_name|| ' COMPILE';
            EXCEPTION
                WHEN OTHERS 
                    THEN v_sqlerrm := sqlerrm;
                         INSERT INTO error_log 
                         VALUES (v_sqlerrm, i.object_name, i.object_type);
						 COMMIT;
        END;    
	END LOOP;
    
	IF v_c = 0 
        THEN DBMS_OUTPUT.put_line('Wszystkie obiekty w schemacie kurs_plsql są skompilowane');    
	END IF;
END skompiluj_nieskompilowane_obiekty;
/

BEGIN
	skompiluj_nieskompilowane_obiekty;
END;
/

SELECT *
FROM error_log;

/*Rozwiń przykład z 10 lekcji tego modułu(przykład 2 w pliku do pobrania) o sprawdzenie czy wartość przekazywana w parametrze rzeczywiście jest nazwą procedury. Jeśli nie to wygeneruj komunikat błędu “Podana nazwa procedury nie istnieje”.
Uwaga. Do sprawdzenia należy użyć słownika all_procedures.*/

DROP PROCEDURE run_procedure;
CREATE OR REPLACE PROCEDURE run_procedure(in_procedure_name VARCHAR2)
IS
v_check_proc INTEGER;

BEGIN
    SELECT COUNT(*)
    INTO v_check_proc
    FROM all_procedures
    WHERE object_name = UPPER(in_procedure_name);

    IF v_check_proc = 0
        THEN RAISE_APPLICATION_ERROR(-20022, 'Podana nazwa procedury nie istnieje');
        ELSE EXECUTE IMMEDIATE 'BEGIN ' ||in_procedure_name|| '; END;';
    END IF;
END run_procedure;
/

BEGIN
    run_procedure('procedura_testowa');
END;
/

/*Napisz blok anonimowy, który będzie tworzył tabelę filie_firmy o dowolnie wybranym przez Ciebie zbiorze kolumn. Blok powinien umożliwiać wielokrotne jego uruchomienie bez zwracania komunikatu błędu, że tabela już istnieje.*/

DECLARE
    ex_tab_exists EXCEPTION;
    PRAGMA EXCEPTION_INIT(ex_tab_exists, -955);
BEGIN
    EXECUTE IMMEDIATE 'CREATE TABLE filie_firmy 
                       (
                            id_filii NUMBER, 
                            nazwa_filii VARCHAR2(100), 
                            nazwa_firmy VARCHAR2(100)
                       )';
EXCEPTION
    WHEN ex_tab_exists 
        THEN NULL;
END;
/

/*Napisz program usuwający wszystkie procedury, funkcje, wyzwalacze i pakiety ze schematu użytkownika kurs_plsql(typ obiektu powinien być przekazywany w parametrze).*/

CREATE OR REPLACE PROCEDURE usun_obiekt(in_obj IN VARCHAR2)
IS
    CURSOR c_obj IS 
                        SELECT object_type, object_name 
                        FROM all_objects
                        WHERE object_type = UPPER(in_obj)
                        AND owner = 'KURS_PLSQL';
                        
BEGIN 
    FOR i IN c_obj
        LOOP
            EXECUTE IMMEDIATE 'DROP ' || i.object_type || ' ' || i.object_name;
        END LOOP; 
END;
/

BEGIN                 
    usun_obiekt('procedure');   
END;
/